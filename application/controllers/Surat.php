<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'vendor/autoload.php';
require_once FCPATH.'application/helpers/NodeLogger.php';


use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007;


class Surat extends MY_Controller {

    function __construct()
    {
        parent::__construct();  
        parent::logon();
        $this->load->model('M_surat') ;
		$this->load->model('M_no_surat');
    }

    public function tambah_submit(){
        $dataPayloadJob = [
                "id" => 12,
    "apply_auto_limit" => false,
    "max_age" => 1800
            ];
        $client = new \GuzzleHttp\Client();
            $server_msg = null;
            $url = "https://dashboard.uisi.ac.id/api/queries/12/results?api_key=UHKfrwuDjwqVWoVm7CgitsaAhOSe6GshcMGFD3zW";
            try {
                $server_output = $client->request('POST',$url, [
                    'json' => $dataPayloadJob                    
                ]);
                $server_msg = json_decode($server_output->getBody()->getContents(),TRUE);
            }catch(Exception $e){
                http_response_code(500);
                echo json_encode([
                    'status' => false,
                    'msg' => 'Gagal export laporan'
                ]);
                exit;
            }
			$get_url = file_get_contents($url);
			$datas = json_decode($get_url);
			$data_array = array($datas);
			$data_mahasiswas = $server_msg['query_result']['data']['rows'];
			$in = 0;
			$i = 0;
			foreach($data_mahasiswas as $data_mahasiswa){
				$dosens[$i]['nama_dosen'] = $data_mahasiswa['pembimbing1'];
				$dosens[$i]['id_dosen'] = $data_mahasiswa['id_pembimbing1'];
				$i++;
			}
			foreach($data_mahasiswas as $data_mahasiswa){
				$dosenq[$in]['nama_dosen'] = $data_mahasiswa['penguji1'];
				$dosenq[$in]['id_dosen'] = $data_mahasiswa['id_penguji1'];
				$in++;
			}
			$dosen = array_unique($dosens,SORT_REGULAR);
			$dosen2 = array_unique($dosenq,SORT_REGULAR);
			// var_dump($dosen);die();
			$data = array(
				'title'         =>  'Surat',
				'css'       =>  'content/surat/css',
				'content'       =>  'content/surat/surats',
				'script'        =>  'content/surat/script',
				'datas_pembimbing' => $dosen,
				'datas_penguji' => $dosen2,
				'all_data' => $data_mahasiswas,
				'kode' => $this->input->post('kode_surat'),
				'tanggal' => $this->input->post('tanggal'),
				) ;


			$this->load->view('template', $data) ;

    }

    public function index()
    {

        $start = $this->input->get('start');
        $q = urldecode($this->input->get('q'));

        if ($q<>'') {
            $config['base_url'] = base_url().'surat/index?q='.urlencode($q);
        } else {
            $config['base_url'] = base_url().'surat/index';
        }

        $config['total_rows'] = $this->M_surat->total_row($q);
        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'start';

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $pagination =  $this->pagination->create_links();
        $hasil = $this->M_surat->get_limit_data($config['per_page'], $start, $q);
        $data = array(
                        'title'         =>  'Surat',
                        'css'       =>  'content/surat/css',
                        'content'       =>  'content/surat/index',
                        'script'        =>  'content/surat/script',
                        'hasil' => $hasil,
                        'pagination' => $pagination,
                        'start' => $start,
                        'total_rows' => $config['total_rows'],
                        'q' => $q,
                        ) ;
        $this->load->view('template', $data) ;  
    }

    

	public function input_surat ()
	{

		$config_validasi = array(
    	[
			'field'=>'tahun_ajaran',
			'label'=>'Tahun Ajaran',
			'rules'=>'required',
			'errors'=>[
				'required' => '%s harap di isi'

			]
		],
            array(
                    'field' => 'kode_surat',
                    'label' => 'Kode Surat',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s harap di isi',
                    ),
            ),
			array(
				'filed' => 'jenis',
				'label'	=> 'jenis',
				'rules' => 'required',
				'errors' => array(
						'required' => '%s harap pilih',
				)
			),
            array(
                    'field' => 'tanggal',
                    'label' => 'Pilih Tanggal',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s ',
                    ),
            ),
        

	    );

	    $this->form_validation->set_rules($config_validasi);
	 if ($this->form_validation->run() == FALSE) {
	    
	     return   $this->tambah_surat();

	    }
	    else
	    {
            $id = $this->input->post('kategori_surat') ;
            $row = $this->M_surat->detail_kategori($id) ;
			$kode_surat = $this->input->post('kode_surat')."/".$this->input->post('tanggal');
	    	$data = array( 
                'kode_surat' => $kode_surat,
                'tanggal' => $this->input->post('tanggal'),
				'jenis_surat' => $this->input->post('jenis'),
				'tahun_ajaran' => $this->input->post('tahun_ajaran'),
				'semester' => $this->input->post('semester'),
            );
            $this->M_surat->input($data);
			$q = $kode_surat;
			$surats = $this->M_surat->by_code($q);
			foreach($surats as $s){
				$id_surat = $s->id_surat;
			}

			$no_surats = $this->M_no_surat->data();
			$no_surat = array();
			$indexx =0;
			foreach($no_surats as $row){
				if($row->jenis_surat == $this->input->post('jenis')){
					$last_number = $row->no;
				}
			}



			/**
			 * Eksekusi Get Data semua semester
			 */
			$client = new \GuzzleHttp\Client();
			$server_msg = null;
			$urlEndPoitSemuaSemester = "https://dashboard.uisi.ac.id/api/queries/14/results?api_key=fUrJIHtluCXs3CBr8tTNBkbokYhMxnTQQWrUf0Zl"; //cari API seluruh data mahasiswa
			try {
				$semester = $data['tahun_ajaran'].'-'.$data['semester'];
				$dataPayloadJobSemuaSemester = [
					"id" => 14,
					"apply_auto_limit" => false,
					"max_age" => 1800,
					"parameters"=>[
					"semester"=>$semester
				]

				];
				$server_output = $client->request('POST',$urlEndPoitSemuaSemester, [
					'json' => $dataPayloadJobSemuaSemester
				]);
				$rs =  json_decode($server_output->getBody()->getContents(),TRUE);
				if(isset($rs['job'])){

					sleep(1);
					$server_output = $client->request('POST',$urlEndPoitSemuaSemester, [
						'json' => $dataPayloadJobSemuaSemester
					]);
					$rs =  json_decode($server_output->getBody()->getContents(),TRUE);

				}


					$nims = [];
				$semester_search = $rs['query_result']['data']['rows'][0]['semester'];
					foreach ($rs['query_result']['data']['rows'] as $data_mahasiswa){
						array_push($nims,$data_mahasiswa['nim']);
					}

			}catch(Exception $e){
				http_response_code(500);
				echo json_encode([
					'status' => false,
					'msg' => 'Gagal export laporan'
				]);
				exit;
			}


			$dataPayloadJob = [
				"id" => 12,
				"apply_auto_limit" => false,
				"max_age" => 1800,

			];
        $client = new \GuzzleHttp\Client();
            $server_msg = null;
            $url = "https://dashboard.uisi.ac.id/api/queries/12/results?api_key=UHKfrwuDjwqVWoVm7CgitsaAhOSe6GshcMGFD3zW"; //cari API seluruh data mahasiswa
            try {
                $server_output = $client->request('POST',$url, [
                    'json' => $dataPayloadJob                    
                ]);
				$rs_all_mahasiswa = json_decode($server_output->getBody()->getContents(),TRUE);

				if(isset($rs_all_mahasiswa['job'])){
					NodeLogger::sendLog($rs);
					NodeLogger::sendLog('send ulang all mahasiswa');

					sleep(1);
					NodeLogger::sendLog('kesekusi lagi');
					$server_output = $client->request('POST',$url, [
						'json' => $dataPayloadJob
					]);
					$rs_all_mahasiswa = json_decode($server_output->getBody()->getContents(),TRUE);

				}
				NodeLogger::sendLog($rs_all_mahasiswa);

			}catch(Exception $e){
                http_response_code(500);
                echo json_encode([
                    'status' => false,
                    'msg' => 'Gagal export laporan'
                ]);
                exit;
            }
			$data_mahasiswas = $rs_all_mahasiswa['query_result']['data']['rows'];
			$mahasiswa_filtered  = [];
			foreach ($data_mahasiswas as $data_mahasiswa){
				if(in_array($data_mahasiswa['nim'],$nims)){
					$data_mahasiswa['semester']=$semester_search;
					array_push($mahasiswa_filtered,$data_mahasiswa);
				}
			}
			NodeLogger::sendLog('Sudah Difilter');
			NodeLogger::sendLog($mahasiswa_filtered);
			$in = 0;
			$i = 0;
			// $nomor_surat_input = $this->input->post('nomor');
			// $arr_nomor = explode("-",$nomor_surat_input);
			$min_number = $this->input->post('nomor');
			// $max_number = $arr_nomor[1];
			// echo $max_number;die();
			if($this->input->post('jenis') == "pembimbing"){
				
				foreach($mahasiswa_filtered as $data_mahasiswa){
					$dosens[$i]['nama_dosen'] = $data_mahasiswa['pembimbing1'];
					$dosens[$i]['id_dosen'] = $data_mahasiswa['id_pembimbing1'];
					$i++;
				}
			}else{
			
				foreach($mahasiswa_filtered as $data_mahasiswa){
					$dosens[$in]['nama_dosen'] = $data_mahasiswa['penguji1'];
					$dosens[$in]['id_dosen'] = $data_mahasiswa['id_penguji1'];
					
					$in++;
				}
			}
			$dosenn = array();
			$dosennn = array();
			$indexd = 0;
			$dosen = array_unique($dosens,SORT_REGULAR);
			
			foreach($dosen as $d){
				$dosenn[$indexd] = $d['id_dosen'];
				$dosennn[$indexd] = $d['nama_dosen'];
				
				$indexd++;
			}
			$numbering = array();
			$indx = 0;
			// for($a=0;$a<=$max_number;$a++){
			// 	if($a >= $min_number){
			// 		$numbering[$indx] = $a;
			// 		$indx++;
			// 	}
			// }
			$a = 0;
			$indexed = 0;
			// var_dump($numbering);die();
			$dosenna = array();
			$dosennaa = array();
			for($i=0;$i<count($dosenn);$i++){
				for($a=0;$a<count($numbering);$a++){
					// echo $i ." - ".$numbering[$a]. " - ";
					if($i == $numbering[$a]){
						$dosenna[$indexed] = $dosenn[$i];
						$dosennaa[$indexed] = $dosenn[$i];
						$indexed++;
					}
				}
			}
			// die();
			$jml_dosen = count($dosenn);
			$nomer = $min_number;
			for($a=0;$a<$jml_dosen;$a++){
				$datas = array( 
					'id_surat' => $id_surat,
					'no' => $nomer,
					'jenis_surat' => $this->input->post('jenis'),
					'id_dosen' => $dosenn[$a],
					'nama_dosen' => $dosennn[$a],
					'semester' => $this->input->post('semester'),
				);
				$nomer++;
				$this->M_no_surat->input($datas);
			}

			// Update No_surat
			$last_numbers = $last_number+1;
			$dataq = array(
				'no_surat' => $min_number." - ".$nomer-1,
			);

			$this->M_surat->update($id_surat,$dataq);

            redirect(base_url('surat'));
	    }
	}
	public function tes(){
		
		$phpWord = new PhpOffice\PhpWord\PhpWord();
		$section = $phpWord->addSection();
		$section->addText('Hello World');
		
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save('php://output');
	}
	public function zip_download($id){
		$tgl = date("Y-m-d");
		$newDate = date('Y-m-d');
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tgl);

        // variabel pecahkan 0 = tahun
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tanggal
		
        $tanggal =  $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
		$surats = $this->M_surat->find($id);

		$dataPayloadJob = [
			"id" => 12,
			"apply_auto_limit" => false,
			"max_age" => 1800
		];
		$client = new \GuzzleHttp\Client();
		/**
		 * Eksekusi Get Data semua semester
		 */
		$client = new \GuzzleHttp\Client();
		$server_msg = null;
		$urlEndPoitSemuaSemester = "https://dashboard.uisi.ac.id/api/queries/14/results?api_key=fUrJIHtluCXs3CBr8tTNBkbokYhMxnTQQWrUf0Zl"; //cari API seluruh data mahasiswa
		try {
			$semester = $surats[0]->tahun_ajaran.'-'.$surats[0]->semester;
			$dataPayloadJobSemuaSemester = [
				"id" => 14,
				"apply_auto_limit" => false,
				"max_age" => 1800,
				"parameters"=>[
					"semester"=>$semester
				]

			];
			$server_output = $client->request('POST',$urlEndPoitSemuaSemester, [
				'json' => $dataPayloadJobSemuaSemester
			]);
			$rs =  json_decode($server_output->getBody()->getContents(),TRUE);
			if(isset($rs['job'])){
				NodeLogger::sendLog($rs);
				NodeLogger::sendLog('send ulang');

				sleep(1);
				NodeLogger::sendLog('kesekusi lagi');
				$server_output = $client->request('POST',$urlEndPoitSemuaSemester, [
					'json' => $dataPayloadJobSemuaSemester
				]);
				$rs =  json_decode($server_output->getBody()->getContents(),TRUE);

			}


			$nims = [];
			$semester_search = $rs['query_result']['data']['rows'][0]['semester'];
			foreach ($rs['query_result']['data']['rows'] as $data_mahasiswa){
				array_push($nims,$data_mahasiswa['nim']);
			}

		}catch(Exception $e){
			http_response_code(500);
			echo json_encode([
				'status' => false,
				'msg' => 'Gagal export laporan'
			]);
			exit;
		}


		$dataPayloadJob = [
			"id" => 12,
			"apply_auto_limit" => false,
			"max_age" => 1800,

		];
		$client = new \GuzzleHttp\Client();
		$server_msg = null;
		$url = "https://dashboard.uisi.ac.id/api/queries/12/results?api_key=UHKfrwuDjwqVWoVm7CgitsaAhOSe6GshcMGFD3zW"; //cari API seluruh data mahasiswa
		try {
			$server_output = $client->request('POST',$url, [
				'json' => $dataPayloadJob
			]);
			$rs_all_mahasiswa = json_decode($server_output->getBody()->getContents(),TRUE);

			if(isset($rs_all_mahasiswa['job'])){
				NodeLogger::sendLog($rs);
				NodeLogger::sendLog('send ulang all mahasiswa');

				sleep(1);
				NodeLogger::sendLog('kesekusi lagi');
				$server_output = $client->request('POST',$url, [
					'json' => $dataPayloadJob
				]);
				$rs_all_mahasiswa = json_decode($server_output->getBody()->getContents(),TRUE);

			}
			NodeLogger::sendLog($rs_all_mahasiswa);

		}catch(Exception $e){
			http_response_code(500);
			echo json_encode([
				'status' => false,
				'msg' => 'Gagal export laporan'
			]);
			exit;
		}
		$data_mahasiswas = $rs_all_mahasiswa['query_result']['data']['rows'];
		$mahasiswa_filtered  = [];
		foreach ($data_mahasiswas as $data_mahasiswa){
			if(in_array($data_mahasiswa['nim'],$nims)){
				$data_mahasiswa['semester']=$semester_search;
				array_push($mahasiswa_filtered,$data_mahasiswa);
			}
		}
		$data_mahasiswas = $mahasiswa_filtered;

		$surat_no = $this->M_no_surat->find($id);
		foreach($surats as $sr){
            $nomor_surat = $sr->no_surat;
			$jenis_surat = $sr->jenis_surat;
			$kode_surat = $sr->kode_surat;
		}
        $ind = 0;
		$files = array();
		$ind = 0;
        foreach($surat_no as $su){
			$semester = $su->semester;
            $id_dosen = $su->id_dosen;
            $nama_dosen = $su->nama_dosen;
			$no_suratt = $su->no;
            $nama_file = $jenis_surat."_".$no_suratt."_".$id_dosen.".docx";
			$filename = 'downloaded/'.$nama_file;
			$files[$ind] = $filename;
			$ind++;
        }

		foreach($surat_no as $row){
			$id_dosen = $row->id_dosen;
			$no_suratt = $row->no;
			
			if (!file_exists($filename)) {
                $surat_nom = $this->M_no_surat->create_file($id);
				// var_dump($surat_nom);die();
				foreach($surat_nom as $no){
                    $index = 0;
                    $mahasiswa = array();
					// echo $no->id_no_surat."<br>";
					$id_no_surat = $no->id_no_surat;
					$id_dosens = $no->id_dosen;
					$nama_dosen = $no->nama_dosen;
					$no_suratt = $no->no;
					if($jenis_surat == "pembimbing"){
						foreach($data_mahasiswas as $data){
							if($data['id_pembimbing1'] == $id_dosens){
								$mahasiswa[$index]["nim"] = $data["nim"];
								$mahasiswa[$index]["nama_mahasiswa"] = $data["nama_mahasiswa"];
								$mahasiswa[$index]["ket"]   = "Pembimbing 1";
								$mahasiswa[$index]["nama_dosen"] = $data["pembimbing1"];
							}elseif($data['id_pembimbing2'] == $id_dosens){
								$mahasiswa[$index]["nim"] = $data["nim"];
								$mahasiswa[$index]["nama_mahasiswa"] = $data["nama_mahasiswa"];
								$mahasiswa[$index]["ket"]   = "Pembimbing 2";
								$mahasiswa[$index]["nama_dosen"] = $data["pembimbing1"];
							}
							$index++;
						}
					}elseif($jenis_surat == "penguji"){
						foreach($data_mahasiswas as $data){
							if($data['id_penguji1'] == $id_dosens){
								$mahasiswa[$index]["nim"] = $data["nim"];
								$mahasiswa[$index]["nama_mahasiswa"] = $data["nama_mahasiswa"];
								$mahasiswa[$index]["ket"]   = "Penguji 1";
								$mahasiswa[$index]["nama_dosen"] = $data["pembimbing1"];
							}elseif($data['id_penguji2'] == $id_dosens){
								$mahasiswa[$index]["nim"] = $data["nim"];
								$mahasiswa[$index]["nama_mahasiswa"] = $data["nama_mahasiswa"];
								$mahasiswa[$index]["ket"]   = "Penguji 2";
								$mahasiswa[$index]["nama_dosen"] = $data["pembimbing1"];
							}
							$index++;
						}
					}
					
					// var_dump($mahasiswa);die();

					$semester = $surats[0]->semester==1?'GANJIL':'GENAP';
					$phpWord = new PhpOffice\PhpWord\PhpWord();
					$section = $phpWord->addSection();
					$phpWord->setDefaultFontName('Calibri');
					$phpWord->addParagraphStyle('center', array('align' => 'center'));
					$section->addText('SURAT TUGAS DOSEN '.strtoupper($surats[0]->jenis_surat),
						array('size'=>18, 'bold'=>true,'underline'=>'single'),'center');
					$section->addText('Nomor: '.str_replace('[NO]', $no->no, $kode_surat),
						array('size'=>11),'center');
					$section->addText('Tentang',
						array('size'=>11),'center');
					$section->addText('PENUGASAN SEBAGAI DOSEN '.strtoupper($surats[0]->jenis_surat).' SKRIPSI PADA SEMESTER '.$semester.' TAHUN',
						array('size'=>12,'bold'=>true),'center');
					$section->addText('AKADEMIK '.$surats[0]->tahun_ajaran.'/'.$surats[0]->tahun_ajaran+1,
						array('size'=>12,'bold'=>true),'center');

					$table = $section->addTable();
					$table->addRow();
					$table->addCell(3000)->addText("Nama dan gelar dosen ", 
						array('size'=>11));
					$table->addCell(500)->addText(":", 
						array('size'=>11));
					$table->addCell(6500)->addText($nama_dosen, 
						array('size'=>11));
					$table->addRow();
					$table->addCell(3000)->addText("Semester", 
						array('size'=>11));
					$table->addCell(500)->addText(":", 
						array('size'=>11));
					$table->addCell(6500)->addText($surats[0]->tahun_ajaran.'/'.($surats[0]->tahun_ajaran+1).' '.$semester,
						array('size'=>11));

					$section->addText('Untuk melaksanakan tugas sebagai dosen '.($surats[0]->jenis_surat).' sebagai berikut :',
						array('size'=>11));
					
					$section->addTextBreak(1);
					
					$styleTable = array('borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80);
					$styleCell = array('valign' => 'center');
					$fontStyle = array('bold' => true, 'align' => 'center');
					$phpWord->addTableStyle('Fancy Table', $styleTable);
					$table = $section->addTable('Fancy Table');
					$table->addRow();
					$table->addCell(100, $styleCell)->addText('No', $fontStyle);
					$table->addCell(2000, $styleCell)->addText('NIM', $fontStyle);
					$table->addCell(8000, $styleCell)->addText('Nama Mahasiswa', $fontStyle);
					$table->addCell(8000, $styleCell)->addText('Keterangan', $fontStyle);
					$no = 1;
						foreach ($mahasiswa as $row) {
							$table->addRow();
							$table->addCell(2000)->addText($no++);
							$table->addCell(2000)->addText($row["nim"]);
							$table->addCell(2000)->addText($row["nama_mahasiswa"]);
							$table->addCell(2000)->addText($row["ket"]);
						}
					$section->addTextBreak(1);
					$section->addText("Kepada saudara yang namanya tercantum dalam surat tugas ini ",
						array('size'=>11));
					$section->addText("Demikian surat tugas ini dibuat agar dapat dilaksanakan ",
						array('size'=>11));
					
					$table = $section->addTable();
					$table->addRow();
					$table->addCell(6500)->addText("");
					$table->addCell()->addText("Dikeluarkan di : ", 
						array('size'=>11));
					$table->addRow();
					$table->addCell(6500)->addText("");
					$table->addCell()->addText("Pada Tanggal : $tanggal",
						array('size'=>11));
					
					$section->addTextBreak(1);

					$table->addRow();
					$table->addCell(6500)->addText("");
					$table->addCell()->addText("Prof. Ir. Joko Rahardjo", 
						array('size'=>11));
					$table->addRow();
					$table->addCell(6500)->addText("");
					$table->addCell()->addText("Wakil Rektor 1", 
						array('size'=>11));
					
					$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
					$nama_file = $jenis_surat."_".$no_suratt."_".$id_dosens.".docx";
					$objWriter->save('downloaded/'.$nama_file);
					$dataw = array(
						'file' => "Y",
					);
					$this->M_no_surat->update($id_no_surat,$dataw);
				}

			}	
		}
		$zipname = $jenis_surat.'_'.$nomor_surat.'.zip';
        if(file_exists($zipname)){
            unlink($zipname);
        }
		$zip = new ZipArchive;
		$zip->open($zipname, ZipArchive::CREATE);
		foreach ($files as $file) {
			$zip->addFile($file);
		}
		$zip->close();
        // die();
		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename='.$zipname);
		header('Content-Length: ' . filesize($zipname));
		readfile($zipname);
		
	}
	public function tambah_surat()
	{
        /*Kode Surat*/

        $cek = $this->M_surat->cek_kode() ;
        if ($cek > 0)
        {
            $row = $this->M_surat->kode();
            $kode = $row->kode ;
        }
        else
        {
            $kode = '000';
        };
			$no_surats = $this->M_no_surat->data();
			
			$indexx =0;
			foreach($no_surats as $row){
				if($row->jenis_surat == 'penguji'){
					$min_no_surat_penguji = $row->no+1;
				}
				if($row->jenis_surat == 'pembimbing'){
					$min_no_surat_pembimbing = $row->no+1;
				}
			}
		
        $noUrut = (int) substr($kode, 0, 3);
        $noUrut++;

        $char = "";
        $kode_surat = $char . sprintf("%03s", $noUrut);
        $tanggal = Date('d') ;
        $bulan = Date('m') ;
        $tahun = Date('Y') ;

        $kategori = $this->M_surat->kategori() ;
		$data = array(
            'kode_surat'         =>  $kode_surat.'/'.$tanggal.'/'.$bulan.'/'.$tahun,
            'kategori'         =>  $kategori,
            'action'         =>  base_url('surat/input'),
            'id_surat' => set_value('id_surat'),
            'tanggal' => set_value('tanggal'),
            'nim' => set_value('nim'),
            'program_studi' => set_value('program_studi'),
            'nama_mahasiswa' => set_value('nama_mahasiswa'),
            'id_kategori' => set_value('id_kategori'),
            'tahun_semester' => set_value('tahun_semester'),
            'semester' => set_value('semester'),
            'min_no_surat_penguji' =>  $min_no_surat_penguji,
            'min_no_surat_pembimbing' => $min_no_surat_pembimbing,
			'title'			=>	'Surat',
			'css'		=>	'content/surat/css',
			'content'		=>	'content/surat/form_surat',
			'script'		=>	'content/surat/script_tambah',
					 ) ;
		$this->load->view('template', $data);
	}
	public function tambah()
	{
        /*Kode Surat*/

        $cek = $this->M_surat->cek_kode() ;
        if ($cek > 0)
        {
            $row = $this->M_surat->kode();
            $kode = $row->kode ;
        }
        else
        {
            $kode = '000';
        };

        $noUrut = (int) substr($kode, 0, 3);
        $noUrut++;

        $char = "";
        $kode_surat = $char . sprintf("%03s", $noUrut);
        $tanggal = Date('d') ;
        $bulan = Date('m') ;
        $tahun = Date('Y') ;

        $kategori = $this->M_surat->kategori() ;
		$data = array(
            'kode_surat'         =>  $kode_surat.'/'.$tanggal.'/'.$bulan.'/'.$tahun,
            'kategori'         =>  $kategori,
            'action'         =>  base_url('surat/input'),
            'id_surat' => set_value('id_surat'),
            'tanggal' => set_value('tanggal'),
            'nim' => set_value('nim'),
            'program_studi' => set_value('program_studi'),
            'nama_mahasiswa' => set_value('nama_mahasiswa'),
            'id_kategori' => set_value('id_kategori'),
            'tahun_semester' => set_value('tahun_semester'),
            'semester' => set_value('semester'),
			'title'			=>	'Surat',
			'css'		=>	'content/surat/css',
			'content'		=>	'content/surat/form',
			'script'		=>	'content/surat/script_tambah',
					 ) ;
		$this->load->view('template', $data);
	}

	public function input ()
	{

		$config_validasi = array(
    	
            array(
                    'field' => 'kode_surat',
                    'label' => 'Kode Surat',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s harap di isi',
                    ),
            ),
        
            array(
                    'field' => 'tanggal',
                    'label' => 'Pilih Tanggal',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s ',
                    ),
            ),
        
            array(
                    'field' => 'kategori_surat',
                    'label' => 'Pilih Kategori',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s ',
                    ),
            ),

        
            array(
                    'field' => 'tahun_semester',
                    'label' => 'Masukkan Tahun Semester',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s',
                    ),
            ),

	    );

	    $this->form_validation->set_rules($config_validasi);
	 if ($this->form_validation->run() == FALSE) {
	    
	        $this->tambah(); 

	    }
	    else
	    {
            $id = $this->input->post('kategori_surat') ;
            $row = $this->M_surat->detail_kategori($id) ;
	    	$data = array( 
                'kode_surat' => $this->input->post('kode_surat'),
                'tanggal' => $this->input->post('tanggal'),
                'id_kategori' => $row->id_kategori,
                'nama_kategori' => $row->nama_kategori,
                'nim' => $this->input->post('nim'),
                'program_studi' => $this->input->post('program_studi'),
                'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
                'tahun_semester' => $this->input->post('tahun_semester'),
                'semester' => $this->input->post('semester'),
            );
            $this->M_surat->input($data);
    		
            redirect(base_url('surat'));
	    }
	}

    public function edit($id)
    {


        $row = $this->M_surat->detail($id) ;

        if($this->input->method()=='post'){
            $id_surat = $this->input->post('id_surat');
            $nomor =  $this->input->post('nomor');
            $no_surats= $this->M_no_surat->find($id_surat);
            //update surat
            $new_nomor = $nomor.' - '.($nomor+count($no_surats)-1);
            $this->M_surat->update($id_surat,['no_surat'=>$new_nomor]);
            $i=0;
            foreach ($no_surats as $no_surat){
                $this->M_no_surat->update($no_surat->id_no_surat,['no'=>($nomor+$i)]);
                $i++;
            }
            redirect(base_url('surat'));
        }



        $data = array(
        'row'=>$row,

                        'title'         =>  'Surat',
                        'css'       =>  'content/surat/css',
                        'content'       =>  'content/surat/edit_surat',
                        'script'        =>  'content/surat/script_tambah'
                        ) ;
        return $this->load->view('template', $data) ;
    }

    public function update ()
    {

        $id = $this->input->post('id_surat') ;
        $config_validasi = array(
        
            array(
                    'field' => 'kode_surat',
                    'label' => 'Kode Surat',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s harap di isi',
                    ),
            ),
        
            array(
                    'field' => 'tanggal',
                    'label' => 'Pilih Tanggal',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s ',
                    ),
            ),
        
            array(
                    'field' => 'kategori_surat',
                    'label' => 'Pilih Kategori',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s ',
                    ),
            ),

        
            array(
                    'field' => 'tahun_semester',
                    'label' => 'Masukkan Tahun Semester',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s',
                    ),
            ),

        );

        $this->form_validation->set_rules($config_validasi);
     if ($this->form_validation->run() == FALSE) {
        
            $this->tambah(); 

        }
        else
        {
            $id_kategori = $this->input->post('kategori_surat') ;
            $row = $this->M_surat->detail_kategori($id_kategori) ;

            $data = array( 
                'kode_surat' => $this->input->post('kode_surat'),
                'tanggal' => $this->input->post('tanggal'),
                'id_kategori' => $row->id_kategori,
                'nama_kategori' => $row->nama_kategori,
                'nim' => $this->input->post('nim'),
                'program_studi' => $this->input->post('program_studi'),
                'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
                'tahun_semester' => $this->input->post('tahun_semester'),
                'semester' => $this->input->post('semester'),
            );
            $this->M_surat->update($id, $data);
            
            redirect(base_url('surat'));
        }
    }


    public function preview($id)
    {
        $row = $this->M_surat->detail($id) ;
        $data = array(
                        'id_surat'      => $row->id_surat,
                        'kode_surat'      => $row->kode_surat,
                        'nim'      => $row->nim,
                        'nama_mahasiswa'      => $row->nama_mahasiswa,
                        'program_studi'      => $row->program_studi,
                        'tanggal'      => $row->tanggal,
                        'id_kategori'      => $row->id_kategori,
                        'nama_kategori'      => $row->nama_kategori,
                        'semester'      => $row->semester,
                        'tahun_semester'      => $row->tahun_semester,
                        'pembimbing1'      => $row->pembimbing1,
                        'pembimbing2'      => $row->pembimbing2,
                        'penguji1'      => $row->penguji1,
                        'penguji2'      => $row->penguji2,
                        'tanggal'      => $row->tanggal,
                        'title'         =>  'Surat',
                        'css'       =>  'content/surat/css',
                        'content'       =>  'content/surat/preview',
                        'script'        =>  'content/surat/script'
                        ) ;
        $this->load->view('content/surat/preview', $data) ;  
    }

    public function hapus($id)
    {
        $this->M_surat->hapus($id);   
        redirect(base_url('surat'));             

        
    }

}
