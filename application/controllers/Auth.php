<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('M_auth');	
	}

	public function index()
	{
		$data = array(
						'title' => 'Login'
					 ) ;
		$this->load->view('login', $data);
	}

	public function authentifikasi ()
	{

        $auth = $this->input->post('auth') ;
        $password = md5($this->input->post('password')) ;
		if (!filter_var($auth, FILTER_VALIDATE_EMAIL)) 
		{ 	
			$type_login = "Username" ;
			$login = $this->M_auth->login_username($auth, $password);
		}
		else
		{
			$type_login = "Email" ;
			$login = $this->M_auth->login_email($auth, $password);
		}

	        if ($login == 1) {
	        	if($type_login == "Username")
	        	{
	            	$row = $this->M_auth->data_username($auth, $password);
	        	}
	        	else
	        	{
	            	$row = $this->M_auth->data_email($auth, $password);
	        	}

	            $data = array(
	                                'logged' => TRUE,
	                                'id_user' => $row->id_user,
	                                'email' => $row->email,
	                                'nama_user' => $row->nama_user,
	                                'level' => $row->level
	                            );
	            $this->session->set_userdata($data);

	            $this->session->set_flashdata('pesan','sukses');
	            redirect(base_url('dashboard'));

	        }
	        else
	        {
	            $this->session->set_flashdata('pesan','gagal');
	            redirect(base_url('auth'));
	        }
	}

	function logout()
	{
	
		$this->session->sess_destroy();
		redirect (base_url('auth'));
	}

	function tes () 
	{
		echo md5('123');
	}
}
