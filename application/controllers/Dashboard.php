<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    function __construct()
    {
        parent::__construct();  
        parent::logon();
    }

	public function index()
	{
		$data = array(
			'jsugas'		=>	$this->db->get('surat')->num_rows(),
			'title'			=>	'Dashboard',
			'css'		=>	'content/dashboard/css',
			'content'		=>	'content/dashboard/index',
			'script'		=>	'content/dashboard/script',
					 ) ;
		$this->load->view('template', $data);
	}
}
