<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MY_Controller {

	function __construct()
	{
		parent::__construct();	
		parent::logon();
        $this->load->model('M_kategori');   
        if($this->session->userdata('level') != 'Admin') 
        {
            redirect(base_url('dashboard')) ;
        }
	}

	public function index()
	{
		$start = $this->input->get('start');
		$q = urldecode($this->input->get('q'));

		if ($q<>'') {
			$config['base_url'] = base_url().'kategori/index?q='.urlencode($q);
		} else {
			$config['base_url'] = base_url().'kategori';
		}

		$config['total_rows'] = $this->M_kategori->total_rows($q);
		$config['per_page'] = 10 ;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'start';

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$pagination =  $this->pagination->create_links();
		$hasil = $this->M_kategori->get_limit_data($config['per_page'], $start, $q);
		$data = array(
						'hasil' => $hasil,
						'pagination' => $pagination,
						'start' => $start,
						'total_rows' => $config['total_rows'],
						'q' => $q,
						'css' => 'content/kategori/css',
						'content' => 'content/kategori/index',
						'script' => 'content/kategori/script'
					 ) ;
		$this->load->view('template', $data);
	}

	public function tambah()
	{
		$data = array(
						'title' => 'Menambahkan kategori',
						'action' => base_url('kategori/input'),
						'id_kategori' => '',
                        'nama_kategori' => set_value('nama_kategori'),
						'css' => 'content/kategori/css',
						'content' => 'content/kategori/form',
						'script' => 'content/kategori/script'
					 ) ;
		$this->load->view('template', $data);
	}

	public function input ()
	{
		$config_validasi = array(
    	
        array(
                'field' => 'nama_kategori',
                'label' => 'Nama Kategori',
                'rules' => 'required',
                'errors' => array(
                        'required' => '%s harap di isi',
                ),
        ),

	    );

	    $this->form_validation->set_rules($config_validasi);
	 if ($this->form_validation->run() == FALSE) {
	    
	        $this->tambah(); 

	    }
	    else
	    {

	    	$data = array( 
                'nama_kategori' => $this->input->post('nama_kategori')
            );
            $this->M_kategori->input($data);
    		
            redirect(base_url('kategori/'));
	    }
	}

    public function detail($id)
    {
        $row = $this->M_kategori->detail($id) ;
        $data = array (
                        'title' => 'Detail Data kategori',
                        'id_kategori' => $row->id_kategori,
                        'nama_kategori' => $row->nama_kategori,
                        'css' => 'content/data/kategori//css',
                        'content' => 'content/data/kategori//detail',
                        'script' => 'content/data/kategori//script'
                        ) ;
        $this->load->view('template', $data) ;
    }

    public function edit($id)
    {
        $row = $this->M_kategori->detail($id) ;
        $data = array (
                        'title' => 'Edit Data kategori',
                        'action' => base_url('kategori/update'),
                        'id_kategori' => $row->id_kategori,
                        'nama_kategori' => $row->nama_kategori,
						'css' => 'content/kategori/css',
						'content' => 'content/kategori/form',
						'script' => 'content/kategori/script'
                        ) ;
        $this->load->view('template', $data) ;
    }


    
    public function update()
    {
        $id = $this->input->post('id_kategori') ;
                
        $config_validasi = array(
        
            array(
                    'field' => 'nama_kategori',
                    'label' => 'Nama Lengkap',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => '%s harap di isi',
                    ),
            ),

        );

                $this->form_validation->set_rules($config_validasi);
             if ($this->form_validation->run() == FALSE) {
                
                    $this->edit($id); 
            
                }
                else{ 

                
                        $data = array(
                                        'nama_kategori' => $this->input->post('nama_kategori')
                                    );
                        $this->M_kategori->update($id, $data);
                        redirect(base_url('kategori'));
              }
    }

    public function hapus($id)
    {
        $this->M_kategori->hapus($id);   
        redirect(base_url('kategori'));             

        
    }

}
