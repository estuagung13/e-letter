<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Admin_model');
		if($this->session->userdata('status') != 'sudah_login') {
			redirect('login');
		}
	}

	public function index() {
		$data = array (
			'title'			=>	'Dashboard',
			'me'			=>	$this->db->get_where('tb_admin',['admin_id' => $this->session->userdata('id')])->row_array(),
			'jsugas'		=>	$this->db->get('tb_surat')->num_rows(),
		);
		$this->load->view('header', $data);
		$this->load->view('index', $data);
		$this->load->view('footer');
	}

	public function surat() {
		$data = array (
			'title'			=>	'Data Surat Tugas',
			'me'			=>	$this->db->get_where('tb_admin',['admin_id' => $this->session->userdata('id')])->row_array(),
			'surat'			=>	$this->Admin_model->data_surat(),
		);
		$this->load->view('header', $data);
		$this->load->view('surat', $data);
		$this->load->view('footer');
	}

	public function surat_add() {
		$data = array (
			'title'			=>	'Input Surat Tugas',
			'me'			=>	$this->db->get_where('tb_admin',['admin_id' => $this->session->userdata('id')])->row_array(),
		);
		$this->form_validation->set_rules('nama', 'nama', 'required', [
					'required'	=>	'Kolom ini tidak boleh kosong']);
		$this->form_validation->set_rules('nip', 'nip', 'required', [
					'required'	=>	'Kolom ini tidak boleh kosong']);
		$this->form_validation->set_rules('prodi', 'prodi', 'required', [
					'required'	=>	'Kolom ini tidak boleh kosong']);
		$this->form_validation->set_rules('sms', 'sms', 'required', [
					'required'	=>	'Kolom ini tidak boleh kosong']);
		$this->form_validation->set_rules('tahun', 'tahun', 'required', [
					'required'	=>	'Kolom ini tidak boleh kosong']);
		if($this->form_validation->run() == FALSE) {
			$this->load->view('header', $data);
			$this->load->view('surat_add', $data);
			$this->load->view('footer');
		}else {
			$this->Admin_model->simpan_surat();
			$this->session->set_flashdata('flash', 'Data baru berhasil ditambahkan');
			redirect('admin/surat');
		}
	}



	public function surat_del($id) {
		$this->db->where('surat_id', $id);
		$this->db->delete('tb_surat');
		$this->session->set_flashdata('flash', 'Data berhasil dihapus');
		redirect('admin/surat');
	}

    //Delete image summernote
    function delete_image(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
}
