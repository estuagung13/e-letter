<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	public function data_surat() {
		$this->db->order_by('created', 'DESC');
		return $this->db->get('tb_surat')->result_array();
	}

	public function simpan_surat() {
		$this->db->order_by('surat_urut', 'DESC');
        $sqlinc = $this->db->get('tb_surat');
        if ($sqlinc->num_rows() == 0) {
        	$autoinc = 1;
        }else {
        	$urutinc = $sqlinc->row()->surat_urut;
        	$urutinc++;
        	$autoinc = $urutinc;
        }
		$this->db->order_by('surat_urut', 'DESC');
        $sql = $this->db->get('tb_surat');
        if ($sql->num_rows() == 0) {
          $surat_kode   = "001"."/57.01/01-01/01-01/".date('m.y');
        }else{
          $noUrut       = substr($sql->row()->surat_kode, 0, 3);
          $noUrut++;
          $surat_kode     = sprintf("%03s", $noUrut)."/57.01/01-01/01-01/".date('m.y');
        }
		$data = array (
			'surat_id'			=>   md5(rand()),
			'surat_kode'		=>   $surat_kode,
			'surat_urut'		=>   $autoinc,
			'surat_nama'		=>   $this->input->post('nama'),
			'surat_nip'			=>   $this->input->post('nip'),
			'surat_prodi'		=>   $this->input->post('prodi'),
			'surat_sms'			=>   $this->input->post('sms'),
			'surat_tahun'		=>   $this->input->post('tahun'),
			'surat_tgl'			=>   date('Y-m-d'),
		);
	
		$this->db->insert('tb_surat', $data);
	}

	public function edit_surat($id) {
		$data = array (
			'surat_nama'		=>   $this->input->post('nama'),
			'surat_nip'			=>   $this->input->post('nip'),
			'surat_prodi'		=>   $this->input->post('prodi'),
			'surat_sms'			=>   $this->input->post('sms'),
			'surat_tahun'		=>   $this->input->post('tahun'),
		);
	
		$this->db->where('surat_id', $id);
		$this->db->update('tb_surat', $data);
	}

	public function set_profil() {
		$sandi = $this->input->post('password');
		$cek = $this->db->get_where('tb_admin',['admin_id' => $this->session->userdata('id')])->row_array();

		if(password_verify($sandi, $cek['admin_password'])) {

			// get foto
		    $config['upload_path'] = 'assets/img/avatars/';
		    $config['allowed_types'] = 'jpg|png|jpeg|gif';
		    $config['encrypt_name'] = TRUE;
		
		    $this->upload->initialize($config);
		    if (!empty($_FILES['gambar']['name'])) {
		        if ( $this->upload->do_upload('gambar') ) {
		            $gambar = $this->upload->data();
		                
		            $data = array(
	                    'admin_nama'			=>	ucwords($this->input->post('nama')),
	                    'admin_email'			=>	strtolower($this->input->post('email')),
						'admin_foto'			=>	$gambar['file_name'],
	                );
	           }
		    }else {
		    	$data = array(
	                'admin_nama'			=>	ucwords($this->input->post('nama')),
                    'admin_email'			=>	strtolower($this->input->post('email')),
					'admin_foto'			=>	$this->input->post('gambar_old'),
		        );
		    }

			$this->db->where('admin_id', $this->session->userdata('id'));
			$this->db->update('tb_admin', $data);
			$this->session->set_flashdata('flash', 'Profil anda berhasil diperbaharui');
			redirect('admin/atur/profil');
		}else {
			$this->session->set_flashdata('error', 'Konfirmasi password salah');
			redirect('admin/atur/profil');
		}
	}

	public function set_password() {
		$sandi = $this->input->post('password');
		$sandi2 = password_hash($this->input->post('password2'), PASSWORD_DEFAULT);
		$cek = $this->db->get_where('tb_admin',['admin_id' => $this->session->userdata('id')])->row_array();

		if(password_verify($sandi, $cek['admin_password'])) {
			$this->db->set('admin_password', $sandi2);
			$this->db->where('admin_id', $this->session->userdata('id'));
			$this->db->update('tb_admin');
			$this->session->set_flashdata('flash', 'Password anda berhasil diperbaharui');
			redirect('admin/atur/password');
		}else {
			$this->session->set_flashdata('error', 'Konfirmasi password lama salah');
			redirect('admin/atur/password');
		}
	}

	public function simpan_surat2() {
		$this->db->order_by('surat_urut', 'DESC');
        $sqlinc = $this->db->get('tb_surat');
        if ($sqlinc->num_rows() == 0) {
        	$autoinc = 1;
        }else {
        	$urutinc = $sqlinc->row()->surat_urut;
        	$urutinc++;
        	$autoinc = $urutinc;
        }
		$this->db->order_by('surat_urut', 'DESC');
        $sql = $this->db->get('tb_surat');
        if ($sql->num_rows() == 0) {
          $surat_kode   = "001"."/57.01/01-01/01-01/".date('m.y');
        }else{
          $noUrut       = substr($sql->row()->surat_kode, 0, 3);
          $noUrut++;
          $surat_kode     = sprintf("%03s", $noUrut)."/57.01/01-01/01-01/".date('m.y');
        }
		$data = array (
			'surat_id'			=>   md5(rand()),
			'surat_kode'		=>   $surat_kode,
			'surat_urut'		=>   $autoinc,
			'surat_isi'		=>   $this->input->post('isi'),
		);
	
		$this->db->insert('tb_surat2', $data);
	}
}