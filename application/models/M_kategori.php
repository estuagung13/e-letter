<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kategori extends CI_Model {

    public function total_rows($q = NULL) 
    {
        $this->db->like('nama_kategori', $q);
        $this->db->from('kategori');
        return $this->db->count_all_results();
    }

    public function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->like('nama_kategori', $q);
        $this->db->limit($limit,$start);
        return $this->db->get('kategori')->result();  
    }


    public function data()
    {
        return $this->db->get('kategori')->result();
    }
    

    public function detail($id)
    {
        $this->db->where('id_kategori', $id);
        return $this->db->get('kategori')->row();
    
    }


    public function input($data)
    {
        $this->db->insert('kategori',$data);
    }



    public function update($id,$data)
    {
        $this->db->where('id_kategori', $id);
        $this->db->update('kategori', $data);
    }



    public function hapus($id)
    {
        $this->db->where('id_kategori', $id);
        $this->db->delete('kategori');
    }





    

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */
