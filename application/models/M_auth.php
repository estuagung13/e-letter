<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {	
    
    function login_email($email,$password) {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query =  $this->db->get('user');
        return $query->num_rows();
    }
    
    function login_username($username, $password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query =  $this->db->get('user');
        return $query->num_rows();
    }
    
    function data_email($email,$password) {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        return $this->db->get('user')->row();
    }
    
    function data_username($username, $password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get('user')->row();
    }
	
}
