<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_no_surat extends CI_Model {


    public function data()
    {
        return $this->db->get('no_surat')->result();
    }
    

    public function detail($id)
    {
        $this->db->where('id_no_surat', $id);
        return $this->db->get('no_surat')->row();
    
    }
	public function find($q = NULL) 
	{
		$this->db->where('id_surat', $q);
        return $this->db->get('no_surat')->result();  
	}

	public function create_file($id){
		$this->db->where('id_surat', $id);
		$this->db->where('file','N');
        return $this->db->get('no_surat')->result();  
	}
    public function input($data)
    {
        $this->db->insert('no_surat',$data);
    }

	public function first_number($id){
		$this->db->where('id_surat',$id);
        return $this->db->get('no_surat')->first();  
	}

	public function last_number($id){
		$this->db->where('id_surat',$id);
        return $this->db->get('no_surat')->last();  
	}

    public function update($id,$data)
    {
        $this->db->where('id_no_surat', $id);
        $this->db->update('no_surat', $data);
    }



    public function hapus($id)
    {
        $this->db->where('id_no_surat', $id);
        $this->db->delete('no_surat');
    }





    

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */
