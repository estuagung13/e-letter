<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_surat extends CI_Model {

    public function total_row($q = NULL) 
    {
        $this->db->like('kode_surat', $q);

        $this->db->join('kategori', 'kategori.id_kategori = surat.id_kategori', 'LEFT');
        $this->db->from('surat');
        return $this->db->count_all_results();
    }

    public function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->like('kode_surat', $q);
        $this->db->limit($limit,$start);
        $this->db->join('kategori', 'kategori.id_kategori = surat.id_kategori', 'LEFT');
        return $this->db->get('surat')->result();  
    }


    public function total_row_penguji($q = NULL) 
    {
        $this->db->like('kode_surat', $q);
        $this->db->where('nama_kategori', 'Penguji');
        $this->db->from('surat');
        return $this->db->count_all_results();
    }

    public function get_limit_data_penguji($limit, $start = 0, $q = NULL)
    {
        $this->db->like('kode_surat', $q);
        $this->db->limit($limit,$start);
        $this->db->where('nama_kategori', 'Penguji');
        return $this->db->get('surat')->result();  
    }


    public function total_row_pengajar($q = NULL) 
    {
        $this->db->like('kode_surat', $q);
        $this->db->where('nama_kategori', 'Pengajar');
        $this->db->from('surat');
        return $this->db->count_all_results();
    }

    public function get_limit_data_pengajar($limit, $start = 0, $q = NULL)
    {
        $this->db->like('kode_surat', $q);
        $this->db->limit($limit,$start);
        $this->db->where('nama_kategori', 'Pengajar');
        return $this->db->get('surat')->result();  
    }


    public function total_row_wali($q = NULL) 
    {
        $this->db->like('kode_surat', $q);
        $this->db->where('nama_kategori', 'Wali');
        $this->db->from('surat');
        return $this->db->count_all_results();
    }

    public function get_limit_data_wali($limit, $start = 0, $q = NULL)
    {
        $this->db->like('kode_surat', $q);
        $this->db->limit($limit,$start);
        $this->db->where('nama_kategori', 'Wali');
        return $this->db->get('surat')->result();  
    }


    public function data()
    {
        return $this->db->get('surat')->result();
    }

    public function kategori()
    {
        return $this->db->get('kategori')->result();
    }

	public function find($q = NULL) 
	{
		$this->db->where('id_surat', $q);

        return $this->db->get('surat')->result();  
	}
	public function min_penguji(){
		$this->db->where('jenis_surat','penguji');
		return $this->db->get('surat')->count();
	}
	public function by_code($q = NULL) 
    {	
        $this->db->where('kode_surat', $q);
        return $this->db->get('surat')->result();  
    }


    public function total_row_dosen($q = NULL) 
    {
        $this->db->like('nama_dosen', $q);
        $this->db->from('dosen');
        return $this->db->count_all_results();
    }

    public function get_limit_data_dosen($limit, $start = 0, $q = NULL)
    {
        $this->db->like('nama_dosen', $q);
        $this->db->limit($limit,$start);
        return $this->db->get('dosen')->result();  
    }


    public function total_row_mahasiswa($q = NULL) 
    {
        $this->db->like('nim', $q);
        $this->db->or_like('nama_mahasiswa', $q);
        $this->db->from('nilaiskripsi');
        return $this->db->count_all_results();
    }

    public function get_limit_data_mahasiswa($limit, $start = 0, $q = NULL)
    {
        $this->db->like('nim', $q);
        $this->db->or_like('nama_mahasiswa', $q);
        $this->db->limit($limit,$start);
        return $this->db->get('nilaiskripsi')->result();  
    }

    
    public function cek_no_surat()
    {
        $query =  $this->db->get('surat');
        return $query->num_rows();
    }

    public function no_surat()
    {
        $this->db->select_max('no_surat', 'no_surat');
        return $this->db->get('surat')->row();
    }


    public function cek_kode()
    {
        $query =  $this->db->get('surat');
        return $query->num_rows();
    }

    public function kode()
    {
        $this->db->select_max('LEFT(kode_surat, 3)', 'kode');
        return $this->db->get('surat')->row();
    }



    public function list_tugas($no_surat)
    {
        $this->db->where('no_surat', $no_surat);
        return $this->db->get('list_tugas')->result();
    
    }


    public function input_list_tugas($data)
    {
        $this->db->insert('list_tugas',$data);
    }

    public function hapus_list_tugas($id)
    {
        $this->db->where('id_list_tugas', $id);
        $this->db->delete('list_tugas');
    }


    public function detail($id)
    {
//        $this->db->where('id_surat', $id);
//        $this->db->join('kategori', 'kategori.id_kategori = surat.id_kategori', 'LEFT');
//        $this->db->join('pembimbingpenguji', 'pembimbingpenguji.nim = surat.nim', 'LEFT');
//        return $this->db->get('surat')->row();
 //       SELECT * FROM `surat` sur inner  join no_surat  nosur on nosur.id_surat=sur.id_surat
//WHERE sur.id_surat = '117'
        $this->db->select('no_surat.*,surat.kode_surat as kode_surat, surat.no_surat as no_surat, surat.tahun_ajaran tahun_ajaran, surat.semester semester, surat.tanggal tanggal');

        $this->db->where('surat.id_surat', $id);
        $this->db->join('no_surat','no_surat.id_surat=surat.id_surat');
        return $this->db->get('surat')->row();

    }



    public function detail_kategori($id)
    {
        $this->db->where('id_kategori', $id);
        return $this->db->get('kategori')->row();
    
    }


    public function input($data)
    {
        $this->db->insert('surat',$data);
    }



    public function update($id,$data)
    {
        $this->db->where('id_surat', $id);
        $this->db->update('surat', $data);
    }



    public function hapus($id)
    {
        $this->db->where('id_surat', $id);
        $this->db->delete('surat');
    }





    

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */
