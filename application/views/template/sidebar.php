<?php $level = $this->session->userdata('level') ; ?>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
          
            <div>
            
                <a href="Dashboard">
                <div class="app-brand demo">
                <img src="<?php echo base_url() ; ?>/assets/img/backgrounds/logo.png" height="65" width=" 200">
                </a>
          
              <i class="bx bx-chevron-left bx-sm align-middle"></i>
            </a>
          </div>

          <div class="menu-inner-shadow"></div>

          <ul class="menu-inner py-1">
            <!-- Dashboard -->
            <li class="menu-item active">
              <a href="Dashboard" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
              </a>
            </li>

            <?php 
              if($level == 'Admin') {
            ?>
            
            <li class="menu-item">
              <a href="<?php echo base_url() ; ?>kategori" class="menu-link">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div data-i18n="Basic">Kategori Surat </div>
              </a>
            </li>

            <?php } ?>
            
            <li class="menu-item">
              <a href="<?php echo base_url() ; ?>surat" class="menu-link">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div data-i18n="Basic">Surat</div>
              </a>
            </li>

           

            
            
            
          </ul>
        </aside>
