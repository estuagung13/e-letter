

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/vendor/libs/jquery/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/vendor/libs/popper/popper.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/vendor/js/bootstrap.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/vendor/js/menu.js"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/vendor/libs/apex-charts/apexcharts.js"></script>

    <!-- Main JS -->
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/js/main.js"></script>

    <!-- Page JS -->
    <script src="https://cdn.jsdelivr.net/gh/rullystudio/assets_sugas/js/dashboards-analytics.js"></script>

    <!-- Place this tag in your head or just before your close body tag. -->
