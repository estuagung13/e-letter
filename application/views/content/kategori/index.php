

          <!-- / Navbar -->

          <!-- Content wrapper -->
          <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <div class="row">
                <div class="col-lg-12 mb-4 order-0">
                  <div class="card">

                      <div class="row">
                        <div class="col-md-6 p-md-4">
                          <h4>Data kategori</h4>
                        </div>
                        <div class="col-md-6 p-md-4 text-end">
                          <a href="<?php echo base_url() ; ?>kategori/tambah" class="btn btn-sm btn-outline-primary">kategori Baru</a>
                        </div>
                      </div>

                      <div class="col-md-12 p-md-4">
                        <table class="table table-hover table-bordered">
                          <tr>
                            <th>
                              No
                            </th>
                            <th>
                              Nama
                            </th>
                            <th>
                              Action
                            </th>
                          </tr>
                          <?php 
                              $no = 1 ;
                              foreach($hasil as $row) {
                          ?>
                          <tr>
                            <td>
                              <?php echo $no++ ; ?>
                            </td>
                            <td>
                              <?php echo $row->nama_kategori ; ?>
                            </td>
                            <td>
                              <a href="<?php echo base_url() ; ?>kategori/edit/<?php echo $row->id_kategori ; ?>">Edit</a> | 
                              <a href="<?php echo base_url() ; ?>kategori/hapus/<?php echo $row->id_kategori ; ?>">Delete</a>
                            </td>
                          </tr>
                          <?php } ?>
                        </table>
                      </div>

                    
                  </div>
                </div>
                
              </div>
              
            </div>
            <!-- / Content -->

            <!-- Footer -->
            