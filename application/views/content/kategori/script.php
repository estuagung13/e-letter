<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.2.1/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

  $(document).on('click','#hapus',function(e){
    e.preventDefault();

         id = $(this).attr('data-id');
         href = "<?php echo base_url() ; ?>administrator/master/kategori/hapus/"+id;
         Swal.fire({
          title: 'Apakah Anda Yakin..?',
          text: "Data Transaksi terpilih akan dihapus..!!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#d33',
          cancelButtonColor: '#3085d6',
          confirmButtonText: 'Hapus Data',
          cancelButtonText: 'Batal'
        }).then((result) => {
        	console.log(href) ;
          if (result.value) {
            document.location.href = href ;

          }
        })
  });

   

  
    var flashData = $('#flash-data').data('flashdata') ;
      if(flashData)
      {

        Swal.fire({
          icon: 'success',
          title: 'Data Transaksi',
          text: 'Berhasil ' + flashData,
          timer: 1500,
          confirmButtonText: 'Beres'
        })
      }

     

});
</script>