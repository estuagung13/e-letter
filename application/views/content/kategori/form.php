

          <!-- / Navbar -->

          <!-- Content wrapper -->
          <div class="content-wrapper">

            <!-- Content -->
            <div class="container-xxl flex-grow-1 container-p-y">
              <div class="row">
                <div class="col-lg-12 mb-4 order-0">
                  <div class="card">
                      <div class="row">
                        <div class="col-md-6 p-md-4">
                          <h2>Kategori Baru</h2>
                        </div>
                        <div class="col-md-6">
                          
                        </div>
                      </div>

                      <div class="col-md-12 p-md-4">

                        <form method="post" action="<?php echo $action ; ?>">
                          <input type="hidden" name="id_kategori" value="<?php echo $id_kategori ; ?>">
                            
                            <div class="mb-4">
                              <label class="form-label">Nama Kategori</label>
                               <select class="form-select" name="nama_kategori">
                                <option value="Penguji" <?php if($nama_kategori == 'Penguji') { ?> Selected <?php } ?> >Penguji</option>
                                <option value="Pengajar" <?php if($nama_kategori == 'Pengajar') { ?> Selected <?php } ?> >Pengajar</option>
                                <option value="Wali" <?php if($nama_kategori == 'Wali') { ?> Selected <?php } ?> >Wali</option>
                                </select>
                              <small class="text-danger"><?php echo form_error('nama_kategori'); ?></small>
                            </div>
                            
                            <div class="mb-4 text-end">
                              <a href="<?php echo base_url() ; ?>kategori" class="btn btn-secondary">Batal</a>
                              <button type="submit" class="btn btn-danger">Simpan</button>
                            </div>

                        </form>

                      </div>


                  </div>
                </div>
                
              </div>
              
            </div>
            <!-- / Content -->

            <!-- Footer -->
            