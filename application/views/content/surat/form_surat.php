

			  <!-- / Navbar -->

			  <!-- Content wrapper -->
			  <div class="content-wrapper">
				<!-- Content -->
				<div class="container-xxl flex-grow-1 container-p-y">
				  <div class="row">
					<div class="col-lg-12 mb-4 order-0">
					  <div class="card">
						  <div class="row">
							<div class="col-md-6 p-md-4">
							  <h2>Buat Surat Tugas Baru</h2>
							</div>
							<div class="col-md-6">

							</div>
						  </div>

						  <div class="col-md-12 p-md-4">
							<form method="post" action="<?php echo base_url().'/surat/input_surat' ; ?>">
							  <input type="hidden" name="id_surat" value="<?php echo $id_surat ; ?>">
								<div class="mb-4">
								  <label class="form-label">Template Kode Surat</label>
								  <input type="text" name="kode_surat" id="kode_surat" class="form-control" value="" required>
								  <small class="text-danger"><?php echo form_error('kode_surat'); ?></small>

																<div class="mb-4">
														<label for="" class="form-label">Jenis Surat</label>
														<select name="jenis" id="jenis" class="form-control"  required>
															<option value="" disabled selected>Pilih Jenis Surat</option>
															<option value="penguji">Penguji</option>
															<option value="pembimbing">Pembimbing</option>
														</select>
													</div>
									<div class="mb-4">
								  <label class="form-label">Nomor Surat</label>
								  <input type="number" name="nomor" id="nomor" class="form-control" value="" placeholder="no surat otomatis terisi sesuai urutan surat yang pernah dibuat (bisa di ubah)" required>
								  <small class="text-danger"><?php echo form_error('nomor_surat'); ?></small>
								</div>
									<div class="mb-4">
										<label class="form-label">Tahun Ajaran</label>
										<input type="number" name="tahun_ajaran" id="nomor" class="form-control" value="" placeholder="Tahun Ajaran" required>
										<small class="text-danger"><?php echo form_error('tahun_ajaran'); ?></small>
									</div>
													<div class="mb-4">
														<label for="" class="form-label">Semester</label>
														<select name="semester" id="jenis" class="form-control"  required>
															<option value="" disabled selected>Pilih Semester</option>
															<option value="1">Ganjil</option>
															<option value="2">Genap</option>
														</select>
													</div>
								<div class="mb-4">
								  <label class="form-label">Tanggal</label>
								  <input class="form-control" type="date" name="tanggal" value="<?php echo $tanggal ; ?>"  required>
								  <small class="text-danger"><?php echo form_error('tanggal'); ?></small>
								</div>




								<div class="mb-4 text-end">
								  <a href="<?php echo base_url() ; ?>surat" class="btn btn-secondary">Batal</a>
								  <button type="submit" class="btn btn-primary">Simpan</button>
								</div>
							  </form>
						  </div>


					  </div>
					</div>

				  </div>

				</div>
				<!-- / Content -->

				<!-- Footer -->
			</div>
					<input type="hidden" id="min_pembimbing" value="<?php echo $min_no_surat_pembimbing; ?>">
					<input type="hidden" id="min_penguji" value="<?php echo $min_no_surat_penguji; ?>">
		  </div>
			  <!-- end Modal mahasiswa -->
			  <script src="https://code.jquery.com/jquery-1.10.0.js" integrity="sha256-iqD4S1Mx78w8tyx9UEwrxuvYYdoAPXLDPfmc5lDUUx0=" crossorigin="anonymous"></script>
			  <script src="<?php echo base_url();?>/assets/js/inputmask/jquery.inputmask.js"></script>
			  <script>
			  $(document).ready(function(){
				// $("#kode_surat").inputmask("99-9999999");
							$("#jenis").change(function(){
									var jenis = $(this).val();
									var min_penguji = $("#min_penguji").val();
									var min_pembimbing = $("#min_pembimbing").val();
									// cons
									if(jenis == "penguji"){
										$("#nomor").val(min_penguji)
										$("#nomor").attr("min",min_penguji);
									}else{
										$("#nomor").val(min_pembimbing)
										$("#nomor").attr("min",min_pembimbing);
									}
							})
			  })
			  </script>
