<!-- / Navbar -->

<!-- Content wrapper -->
<div class="content-wrapper">
	<!-- Content -->

	<div class="container-xxl flex-grow-1 container-p-y">
		<div class="row">
			<div class="col-lg-12 mb-4 order-0">
				<div class="card">

					<div class="row">
						<div class="col-md-6 p-md-4">
							<h4>Buat Surat Tugas Baru</h4>
						</div>
						<div class="col-md-6 p-md-4 text-end">
							<a href="<?php echo base_url(); ?>surat/tambah_surat"
							   class="btn btn-sm btn-outline-primary">Buat surat tugas</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">

						</div>
						<div class="col-md-4"><!--
                          <form method="get" action="<?php echo base_url(); ?>surat/index">
                            <div class="input-group">
                              <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search for">
                              <button type="submit" class="btn btn-primary" id="browse_mahasiswa">Cari</button>
                            </div>
                          </form> -->
						</div>
					</div>
					<div class="col-md-12 p-md-4">


						<table class="table table-hover mt-5">
							<tr>
								<th>
									No
								</th>
								<th>
									Tanggal
								</th>
								<th>
									Kode
								</th>
								<th>
									Jenis Surat
								</th>
								<th>
									Tahun Ajaran
								</th>
								<th>
									Semester
								</th>
								<th>
									Nomor Surat
								</th>
								<th>
									Action
								</th>
							</tr>
							<?php
							foreach ($hasil as $row) {
								?>
								<tr>
									<td>
										<?php echo ++$start; ?>
									</td>
									<td>
										<?php echo tgl_indo($row->tanggal); ?>
									</td>
									<td>
										<?php echo $row->kode_surat; ?>
									</td>
									<td>
										<?php echo $row->jenis_surat; ?>
									</td>
									<td>
										<?php echo $row->tahun_ajaran ?>
									</td>
									<td>
										<?php echo ($row->semester == 1) ? 'Ganjil' : 'Genap'; ?>
									</td>
									<td>
										<?php echo $row->no_surat; ?>
									</td>
									<td>
										<a href="<?php echo base_url(); ?>surat/zip_download/<?php echo $row->id_surat; ?>">Download</a>
										|

										<a href="<?php echo base_url(); ?>surat/edit/<?php echo $row->id_surat; ?>">Edit</a>
										|
										<a href="<?php echo base_url(); ?>surat/hapus/<?php echo $row->id_surat; ?>">Hapus</a>
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>

					<div class="col-md-12 text-end">
						<?php echo $pagination; ?>
					</div>


				</div>
			</div>

		</div>

	</div>
	<!-- / Content -->

	<!-- Footer -->
