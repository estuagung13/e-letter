<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();
		//$this->load->model('M_user');	
	}

	public function logon()
	{
		if($this->session->userdata('logged') == '') {
			redirect(base_url('auth'));
		}
	}
	
}
