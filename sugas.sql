-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2022 at 02:13 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sugas`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(50) NOT NULL,
  `nama_kategori` varchar(225) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(100002, 'Penguji'),
(100003, 'Pengajar'),
(100004, 'Wali');

-- --------------------------------------------------------



--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(50) NOT NULL,
  `no_surat` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `kode_surat` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `id_kategori` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `nama_kategori` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `tanggal` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `nim` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `nama_mahasiswa` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `program_studi` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `tahun_semester` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `semester` varchar(225) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id_surat`, `no_surat`, `kode_surat`, `id_kategori`, `nama_kategori`, `tanggal`, `nim`, `nama_mahasiswa`, `program_studi`, `tahun_semester`, `semester`) VALUES
(100010, NULL, '001/21/06/2022', '100002', 'Penguji', '2022-06-21', '1011510135', 'Lucy Thio Martha Siburian', 'Manajemen', '2020-2021', 'GANJIL'),
(100011, NULL, '002/21/06/2022', '100003', 'Pengajar', '2022-06-22', '1011610060', 'Muhammad Irfan Hidayatulloh', 'Manajemen', '2020-2021', 'GANJIL');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(50) NOT NULL,
  `nama_user` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `username` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `password` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `level` varchar(225) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(225) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `email`, `username`, `password`, `level`, `status`) VALUES
(100001, 'Admin', 'admin@gmail.com', 'Admin', '202cb962ac59075b964b07152d234b70', 'Admin', 'Aktif'),
(100002, 'User', 'user@gmail.com', 'User', '202cb962ac59075b964b07152d234b70', 'User', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100005;

--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100012;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100003;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
